'use strict';

angular.module('common.ui.alerts', [])
.directive('alerts', ['$rootScope', function ($rootScope) {
    return {
        restrict: 'E',
        template: '<alert ng-repeat="alert in alerts"></alert>',
        controller: controller,
        link: link
    };

    function controller($scope) {
        var ctrl = this;
        var alerts = $scope.alerts = [];

        /**
         * @method addAlert
         * @desc Prepends alert data to the beginning of the alerts array
         * @param data [Object] details about the alert
         */
        ctrl.addAlert = function addAlert(data) {
            alerts.unshift(data);
        }

        /**
         * @method closeAlert
         * @desc Removes alert from alerts array
         * @param $index [Number] alert index to be removed in the alerts array
         */
        ctrl.closeAlert = function closeAlert($index) {
            alerts.splice($index, 1);
        };

        /**
         * @method getCount
         * @desc Returns the number of alerts visible
         * @return [Number] The count of visible alerts
         */
        ctrl.getCount = function getCount() {
            return alerts.length;
        }
    }

    function link(scope, element, attrs, alertsCtrl) {

        // alerts are triggered via the 'alert'
        // event.
        $rootScope.$on('alert', function (event, data) {
            alertsCtrl.addAlert(data);
        });
    }
}])


//---
// Alert
// data attributes
// message [String] - Message to user
// type [String] - Type of message [success, warning, error, info]
// id [String] - The current index of the alert in the alert array
// title (optional)[String] - The title of the alert. If not defined, title will default to type.
// dismissTimeout (optional)[Boolean] flag to disable auto dismissal.
//     TRUE - disable auto timeout;
//     FALSE || UNDEFINED - alert removed by auto timeout.
// timeout (optional)[Number] - the number of millisecs to dismiss the alert (defaults 5000ms).
//---
.directive('alert', ['$timeout', '$sce', function ($timeout, $sce) {
    return {
        restrict: 'E',
        require: '^alerts',
        replace: true,
        templateUrl: '/partials/alert.html',
        scope: {
            id: '=',
            data: '='
        },
        link: link
    };

    function link(scope, element, attrs, alertsCtrl) {

        // Timeout defaults to 5 seconds if not specified for alert.
        var timeout = scope.data.timeout || 5000;

        // Auto dismissal is default, unless specified by the user
        if (!scope.data.dismissTimeout) {
            $timeout(function () {
                alertsCtrl.closeAlert(parseInt(scope.id, 10));
            }, timeout);
        }

        // Allow HTML to be rendered in alert message.
        scope.trustedMessage = $sce.trustAsHtml(scope.data.message);

        //---
        // @method closeAlert
        // @desc Manual close for an alert.
        // #param [Number] the current index of the alert in the alert array
        //---
        scope.close = function closeAlert($index) {
            alertsCtrl.closeAlert(parseInt(scope.id, 10));
        }
    }
}]);
